import {LitElement, html} from 'lit';
 
class PersonaStats extends LitElement{
   
    static get properties() {
       return {
           people : {type: Array}
       };
   }

   constructor() {
       super();

       this.people = [];

   }

   updated(changedProperties){
        console.log("updated en persona-stats");

        if(changedProperties.has("people")){
            console.log("Has cambiado el valor de la propiedad PEOPLE en persona-stats");

            let peopleStats = this.gatherPeopleArrayInfo(this.people);

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people-stats",
                    {
                        detail: {
                            peopleStats: peopleStats
                        }
                    }
                )
            )
        }
    }

    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo");

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        let maxYearsInCompany = 0;

        people.forEach(
            person => {
                if (person.yearsInCompany > maxYearsInCompany) {
                    maxYearsInCompany = person.yearsInCompany;
                }
            }
        )
            console.log("maxYearsInCompany es " + maxYearsInCompany);
            peopleStats.maxYearsInCompany = maxYearsInCompany;


        console.log("peopleStats " + peopleStats );

        return peopleStats;
    }
}


customElements.define("persona-stats", PersonaStats);