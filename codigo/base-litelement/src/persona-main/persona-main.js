 import {LitElement, html} from 'lit';
 import '../persona-ficha-listado/persona-ficha-listado.js';
 import '../persona-form/persona-form.js';
 import '../persona-sidebar/persona-sidebar.js';
 import '../persona-main-dm/persona-main-dm.js';
 
 class PersonaMain extends LitElement{
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            maxYearsinCompanyFilter: {type: Number}
        };
    }

    constructor(){
        super();

        this.people = [];
        this.showPersonForm = false;
        this.maxYearsinCompanyFilter = 0;

    }
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas - Main</h2>
            <br />
            <div class="row" id="peopleList">
                <main class="row row-cols-1 row-cols-sm-4">
                ${this.people.filter(
                        person => person.yearsInCompany <= this.maxYearsinCompanyFilter
                ).map(
                        person => html` <persona-ficha-listado
                                            fname="${person.name}"
                                            yearsInCompany="${person.yearsInCompany}"
                                            profile="${person.profile}"
                                            .photo="${person.photo}"
                                            @delete-person="${this.deletePerson}"
                                            @info-person="${this.infoPerson}"                                         
                                        ></persona-ficha-listado>
                                        `                
                    )
                }
                </main>
            </div>
            <div class="row">
                <persona-form
                id="personForm"
                class="d-none border rounded border-primary"
                @persona-form-clase="${this.personFormClose}"
                @persona-form-store="${this.personFormStore}"></persona-form>
            </div>
            <persona-main-dm @updated-people="${this.updatedPeople}"></persona-main-dm>
        `
    }

    updated(changedProperties){
        console.log("updated en persona-main");

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha  cambiado el valor de la propiedad SHOWPERSONFORM en persona-main");
            
            if (this.showPersonForm === true) {
                this.showPersonFromData();
            } else {
                this.showPersonList();
            }
        
        }

        if (changedProperties.has("people")) {
            console.log("Ha  cambiado el valor de la propiedad PEOPLE en persona-main");

            this.dispatchEvent(
                new CustomEvent(
                    "people-updated",
                    {
                        detail : {
                            people:this.people
                        }
                    }
                )
            )
        }

        if (changedProperties.has("maxYearsinCompanyFilter")) {
            console.log("Ha  cambiado el valor de la propiedad maxYearsinCompanyFilter en persona-main");
            console.log("Se va a mostrar las personas de antiguedad maxima: " + this.maxYearsinCompanyFilter);
        }


    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        //console.log(e.detail);
        console.log("Se va borrar la persona " + e.detail.name);
        
        //BORRAR
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
        //ACTUALIZAR - this.people[indexOfPerson] = e.detail.person;
        //AÑADIR - this.people.push(e.detail.person);

    }

    infoPerson(e) {
        console.log("infoPerson en persona-main");
        console.log("Se ha pedido más información de " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;

    }


    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrar listado personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFromData() {
        console.log("showPersonFormData");
        console.log("Mostrar listado personas");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    personFormClose(e) {
        console.log("personFormClose");        
        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore");
        console.log(e.detail);
        //console.log(e.detail.person); / console.log(e.detail.editingPerson);

        if (e.detail.editingPerson === true) {
            console.log("se va actualizar la persona de nombre: " + e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person
                );

        } else {
            console.log("Se va almacenar  una persona nueva") ;
            this.people = [...this.people, e.detail.person]; // ... JS SPREAD SYNTAX
        }

        console.log("Persona terminado");
        this.showPersonForm = false;
    }

    updatedPeople(e){
        console.log("updatedPeople en persona-main");

        this.people = e.detail.people;
    }
 }

 customElements.define("persona-main", PersonaMain);