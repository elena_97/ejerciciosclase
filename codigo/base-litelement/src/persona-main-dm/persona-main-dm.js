import {LitElement, html} from 'lit';
 
class PersonaMainDm extends LitElement{

   static get properties() {
       return {
           people: {type: Array}
       };
   }

   constructor() {
       super();

       this.people = [
        {
            name: "Dobby",
            yearsInCompany: 6,
            photo: {
                src: "./img/dobby.jpg",
                alt: "Dobby, el elfo"
            },
            profile: " Dobby es un elfo libre "
        },{
            name: "Lisa",
            yearsInCompany: 8,
            photo: {
                src: "./img/lisa.jpg",
                alt:"Lisa Simpson"
            },
            profile: " De la sociedad aprendí que es mejor ponerse los audífonos"
        },{
            name: "Miercoles",
            yearsInCompany: 10,
            photo: {
                src: "./img/miercoles.jpg",
                alt:"Miercoles Addams"
            },
            profile: " ¿ Crees en el mal ? "
        },{
            name: "Leia",
            yearsInCompany: 12,
            photo: {
                src: "./img/leia.jpg",
                alt:"Princesa Leia"
            },
            profile: " Ayúdame Obi-Wan Kenobi, eres mi única esperanza"
        },{
            name: "Peter",
            yearsInCompany: 8,
            photo: {
                src: "./img/peterpan.jpg",
                alt:"Peter Pan"
            },
            profile: " Vivir. Esa será mi mejor aventura"
        }
        
    ];
   }

   updated(changedProperties){
        console.log("updated en persona-main-dm");

        if (changedProperties.has("people")) {
            console.log("ha cambiado el valor de la propiedad PEOPLE en persona-main-dm");

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }
   }

}

customElements.define("persona-main-dm", PersonaMainDm);