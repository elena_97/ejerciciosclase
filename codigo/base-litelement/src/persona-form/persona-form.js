import {LitElement, html} from 'lit';
 
class PersonaForm extends LitElement{
   static get properties() {
       return {
        person: {type: Object},
        editingPerson: {type: Boolean}
       };
   }

   constructor() {
       super();

        this.resetFormData();

   }
   
   render() {
       return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo: </label>
                        <input @input="${this.updateName}"
                            type="text" class="form-control" placeholder="Nombre Completo"
                            .value="${this.person.name}"
                            ?disabled="${this.editingPerson}"
                        />
                    </div>
                    <br>
                    <div class="form-group">
                        <label>Perfil: </label>
                        <textarea @input="${this.updateProfile}"
                            class="form-control" placeholder="Perfil" rows="5"
                            .value="${this.person.profile}"
                        >
                        </textarea>
                    </div>
                    <br>
                    <div class="form-group">
                        <label>Años: </label>
                        <input @input="${this.updateAños}"
                            type="number" class="form-control" placeholder="Años"
                            .value="${this.person.yearsInCompany}"
                        />
                    </div>
                    <br>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>ATRÁS</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>GUARDAR</strong></button>
                </form>
            </div>
       `
   }

   goBack(e){
        console.log("goBack");

        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-clase", {}));
        this.resetFormData();

    }

   storePerson(e){
        console.log("storePerson");
        
        e.preventDefault();

        console.log("propiedad NAME vale " + this.person.name);
        console.log("propiedad PROFILE vale " + this.person.profile);
        console.log("propiedad AÑOS vale " + this.person.yearsInCompany);

        this.person.photo = {
            src: "./img/dobby.jpg",
            alt: "Persona"
        };

        this.dispatchEvent(
            new CustomEvent(
                "persona-form-store",
                {
                    detail: {
                        person: {
                            name: this.person.name,
                            profile: this.person.profile,
                            yearsInCompany: this.person.yearsInCompany,
                            photo: this.person.photo
                        },
                        editingPerson: this.editingPerson
                    }
                }
                
            )
        )

    }

    updateName(e){
        console.log("updateName");
        console.log("actualizando la propiedad name con valor " + e.target.value);

        this.person.name = e.target.value;
        
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("actualizando la propiedad profile con valor " + e.target.value);

        this.person.profile = e.target.value;
    }

    updateAños(e){
        console.log("updateAños");
        console.log("actualizando la propiedad años con valor " + e.target.value);

        this.person.yearsInCompany = e.target.value;
    }

    resetFormData() {
        console.log("resetFormData");

        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;

    }
        
    



}

customElements.define("persona-form", PersonaForm);